using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private float jumpForce = 10f;
    [SerializeField] private Collider2D feetCollider;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private string groundLayer = "Ground";

    private Rigidbody2D playerRigidbody2D;
    private Animator playerAnimator;
    private SpriteRenderer playerSpriteRenderer;
    private bool isGrounded;

    private void Awake()
    {
        playerRigidbody2D = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        SwitchAnimation(playerInput);
        Flip(playerInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask(groundLayer));
        if (Input.GetKeyDown(jumpButton) && isGrounded)
        {
            Jump();
        }
    }

    private void Move(float direction)
    {
        playerRigidbody2D.velocity = new Vector2(direction * speed, playerRigidbody2D.velocity.y);
    }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody2D.velocity.x, jumpForce);
        playerRigidbody2D.velocity = (jumpVector);
    }

    private void SwitchAnimation(float playerInput)
    {
        playerAnimator.SetBool("Run", playerInput != 0);
    }

    private void Flip(float playerInput)
    {
        if (playerInput < 0)
        {
            playerSpriteRenderer.flipX = true;
        }
        if (playerInput > 0)
        {
            playerSpriteRenderer.flipX = false;
        }
    }
}
